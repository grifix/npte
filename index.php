<?php
require_once "View.php";

$v = 'layout.php';
if(isset($_GET['v'])){
    $v = basename($_GET['v']);
}

$view = new View($v);
echo $view->render([
    'text' => 'It works!'
]);


