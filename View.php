<?php
/**
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class View
{
    protected $path;
    protected $vars;
    protected $blocks;
    /**
     * @var View
     */
    protected $parent;
    protected $currentBlock;
    
    public function __construct($path, array $vars = [])
    {
        $this->path = $path;
        $this->vars = $vars;
    }
    
    protected function inherits($path)
    {
        $this->parent = new View($path);
    }
    
    public function setBlocks(array $blocks){
        $this->blocks = $blocks;
    }
    
    public function render(array $vars = [])
    {
        $this->vars = $vars;
        ob_start();
        require $this->path;
        
        
        if (!$this->parent) {
            return ob_get_clean();
        } else {
            ob_get_clean();
            
            $this->parent->setBlocks($this->blocks);
            return $this->parent->render($this->vars);
        }
    }
    
    protected function block($blockName)
    {
        $this->currentBlock = $blockName;
        if (!$this->parent && isset($this->blocks[$blockName])) {
            echo $this->blocks[$blockName];
        }
        ob_start();
    }
    
    protected function endBlock()
    {
        if (!$this->parent) {
            if (isset($this->blocks[$this->currentBlock])) {
                ob_end_clean();
            } else {
                echo ob_get_clean();
            }
        } else {
            $this->blocks[$this->currentBlock] = ob_get_clean();
        }
        $this->currentBlock = null;
    }
}