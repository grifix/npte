<?php /**@var $this View*/?>
<html>
    <head>
        <title>
            <?php $this->block('title')?>Layout title<?php $this->endBlock()?>
        </title>
    </head>
    <body>
        <div class="content">
            <?php $this->block('content')?>
            Layout content
            <?php $this->endBlock()?>
        </div>
        
        <div class="footer">
            <?php $this->block('footer')?>
            Layout footer
            <?php $this->endBlock()?>
        </div>
    </body>
</html>
